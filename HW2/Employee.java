import java.io.Serializable;
public class Employee implements Serializable {
    private String id; // เลขประจําตวัพนกังาน
    private String firstName; // ชืÉอ
    private String lastName; // นามสกลุ
    private double salary; // เงินเดือน

    public Employee(String idVal, String fnVal, String lnVal, double slrVal) {
        id = idVal; firstName = fnVal; lastName = lnVal; salary = slrVal;
    }
    public String getID() { return id; }
    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public double getSalary() { return salary; }
    public void setSalary(double newSalary) { salary = newSalary; }
    public String toString() {
        return "ID: " + id + ": " + firstName + " " + lastName + " Salary = " + salary;
    }
} 