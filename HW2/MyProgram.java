import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class MyProgram {
    public static void main(String[] args) throws IOException {
        File f = new File(args[0]);
        if(!f.exists()){
            throw new FileNotFoundException("File Not Found");
        }
        RandomAccessFile rf = new RandomAccessFile(f,"rw");
        long lenFile = rf.length();
        long item = lenFile/4;
        long i = 0;
        while (i<item){
            int x = (int) rf.readFloat();
            System.out.println(x);
            rf.seek(rf.getFilePointer()-4);
            rf.writeFloat(x);
            i++;
        }
        rf.close();
    }
}
