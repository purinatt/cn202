
import java.util.*;

public class TestRunningTime {
    public static double[] prefixAvg1(double[] x) {
        double[] a = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            double temp = 0;
            for (int j = 0; j <= i; j++) {
                temp = temp + x[j];
            }
            a[i] = temp / (i + 1);
        }
        return a;
    }

    public static double[] prefixAvg2(double[] x) {
        double a[] = new double[x.length];
        double s = 0;
        for (int i = 0; i < x.length; i++) {
            s = s + x[i];
            a[i] = s / (i + 1);
        }
        return a;
    }

    public static void main(String[] args) {
        int n;
        for (n = 2000; n <= 10000; n += 1000) {
            System.out.println("n = " + n);
            final int ROUND = 5;
            double[] x = new double[n];
            double[] a;
            long startTime;
            long endTime;
            long sumPrefixAvg1 = 0;
            long sumPrefixAvg2 = 0;
            double avgTime1;
            double avgTime2;
            for (int i = 0; i < ROUND; i++) {
                Random r = new Random();
                for (int j = 0; j < n; j++) {
                    x[j] = r.nextDouble();
                }
                //prefixAvg1
                startTime = System.nanoTime();
                a = prefixAvg1(x);
                endTime = System.nanoTime();
                sumPrefixAvg1 = sumPrefixAvg1 + (endTime - startTime);


                //prefixAvg2
                startTime = System.nanoTime();
                a = prefixAvg2(x);
                endTime = System.nanoTime();
                sumPrefixAvg2 = sumPrefixAvg2 + (endTime - startTime);

            }

            double avg1 = sumPrefixAvg1 / 5.0;
            double avg2 = sumPrefixAvg2 / 5.0;
            System.out.println("Avg. Time of PrefixAvg1 : " + avg1);
            System.out.println("Avg. Time of PrefixAvg2 : " + avg2);

        }
    }
}
