import java.io.*;
import java.util.ArrayList;

public class EmployeeFile {
    public static void addEmployee(Employee e,String  employeeFileName)throws IOException, ClassNotFoundException {
        File f = new File(employeeFileName);
        if(f.exists()){
            FileInputStream input = new FileInputStream(f);
            ObjectInputStream obj = new ObjectInputStream(input);
            ArrayList<Employee> aEmployee;
            aEmployee = (ArrayList<Employee>) (obj.readObject());
            obj.close();
            aEmployee.add(e);
            FileOutputStream output = new FileOutputStream(employeeFileName);
            ObjectOutputStream out = new ObjectOutputStream(output);
            out.writeObject(aEmployee);
            out.close();
        }
        else{
            FileOutputStream out = new FileOutputStream(employeeFileName);
            ObjectOutputStream outObj = new ObjectOutputStream(out);
            ArrayList<Employee> aEmployee = new ArrayList<>();
            aEmployee.add(e);
            outObj.writeObject(aEmployee);
            outObj.close();
        }
    }
    public static boolean print(String employeeFileName)throws IOException, ClassNotFoundException {
        File f = new File(employeeFileName);
        if(f.exists()){
            FileInputStream in = new FileInputStream(f);
            ObjectInputStream obj = new ObjectInputStream(in);
            ArrayList<Employee> aEmployee = (ArrayList<Employee>) obj.readObject();
            for(Employee e : aEmployee){
                System.out.println(e);
            }
            return true;
        }
        else{
            return false;
        }
    }

}
