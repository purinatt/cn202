
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


public class FindAdditiveInverse {

    public static void main(String[] args) throws IOException {
        FileReader input = new FileReader(args[0]);
        FileWriter output = new FileWriter(args[1]);
        Scanner in = new Scanner(input);
        PrintWriter out = new PrintWriter(output);
        
        while(in.hasNextLine()){
            String sLine = in.nextLine();
            Scanner ln = new Scanner(sLine);
            while(ln.hasNextInt()){
                int x = ln.nextInt();
                x = x * -1;
                out.print(x + " ");           
            }
            out.println();
        }        
        input.close();
        output.close();
        
    }
}
