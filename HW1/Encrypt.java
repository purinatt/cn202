
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Encrypt {
    public static void usage(){
        System.out.println("Usage: java Encrypt inputTextFile OutputTextFile");
        System.exit(1);
    }
    public static String encrypt(String s){
        String output = "";
        for(int i=0;i<s.length();i++){
            char x = s.charAt(i);
            if(x>='A' && x<='Z'){
                x = (char)('z'-(x-'A')); 
            }
            else if(x>='a' && x<='z'){
                x = (char)('Z'-(x-'a'));
            }
            else if(x>='0' && x<='9'){
                x = (char)('9'-(x-'0'));
            }
    
            output = output + x;
        }
        return output;
    } 
    public static void main(String[] args) throws IOException {
        if(args.length<2){
            usage();
        }
        FileReader input = new FileReader(args[0]);
        FileWriter output = new FileWriter(args[1]);
        Scanner in = new Scanner(input);
        PrintWriter out = new PrintWriter(output);
        
        while(in.hasNextLine()){
            String sLine = in.nextLine();
            //System.out.println(sLine);
            String oLine = encrypt(sLine);
            out.println(oLine);
        }
        input.close();
        output.close();
        
    }
}
