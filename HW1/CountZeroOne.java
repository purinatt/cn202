
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


public class CountZeroOne {
    public static void usage(){
        System.out.println("Usage: java CountZeroOne inputTextFile OutputTextFile ");
        System.exit(1);
    }
    public static void main(String[] args) throws IOException {
        FileReader input = new FileReader(args[0]);
        FileWriter output = new FileWriter(args[1]);
        Scanner in = new Scanner(input);
        PrintWriter out = new PrintWriter(output);
        
        while(in.hasNextLine()){
            String sLine = in.nextLine();
            int count0 = 0;
            int count1 = 0;
            for(int i = 0;i<sLine.length();i++){
                char c = sLine.charAt(i);
                if(c=='0'){
                    count0++;
                }
                else if(c=='1'){
                    count1++;
                }
            }
            out.println(count0 + " " + count1);
        }
        input.close();
        output.close();
    }
}
