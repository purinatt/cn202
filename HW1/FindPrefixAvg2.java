
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


public class FindPrefixAvg2 {
    public static void usage(){
        System.out.println("Usage: java FindPrefixAvg inputTextFile OutputTextFile ");
        System.exit(1);
    }
    public static void main(String[] args) throws IOException {
        if(args.length != 2){
            usage();
        }
        FileReader input = new FileReader(args[0]);
        FileWriter output = new FileWriter(args[1]);
        Scanner in = new Scanner(input);
        PrintWriter out = new PrintWriter(output);
        int count = 1;
        double sum = 0;
        double avg;
        while(in.hasNext()){
            double x = in.nextDouble();
            System.out.println("x => " + x);
            sum = sum + x;
            avg = sum/count;
            System.out.println("sum => " + sum);
            System.out.println("count => " + count);
            System.out.println(avg);
            out.println(avg + "");
            count++;
        }
        
    }
}
